﻿#pragma strict

var scoreCounter : int;
var stoneCount : int;
var plasticCount : int;
var jarCount : int;
var tempTime : float;
var timetime : float;
var tempString : String;
var gameEnded : boolean;
var skin : GUISkin;

function Start () {
	scoreCounter = 2310;
	stoneCount = 8;
	plasticCount = 6;
	jarCount = 14;
	timetime = 10;
	tempTime = Time.time;
	tempString = "";
	gameEnded = true;
	
}

function Update () {
	
}

function digitCheck() {
	var multiplier : int = 1;
	while (scoreCounter * multiplier < 1000000)
	{
		multiplier *= 10;
	}
	tempString = multiplier + "";
	multiplier = 0;
	tempString = tempString.Substring(1, tempString.Length - 1);
}

function OnGUI() {
	GUI.skin = skin;
	if (gameEnded)
	{
		GUI.Label(new Rect(Screen.width / 7 * 1.7, Screen.height / 2.4, 380, 100), "Your total SCORE \n is " + GameUI.scoreCounter);
	}                 
}