﻿#pragma strict

static var isTouching : boolean;
var speed : float = 1;

function Start () {
	isTouching = false;
}

function Update () {
	if (Input.touchCount > 0 && 
        Input.GetTouch(0).phase == TouchPhase.Moved) {
        
        var hit : RaycastHit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint((Input.GetTouch(0).position)), Vector2.zero);
	
		var touch : Touch = Input.GetTouch(0);
	
        // Get movement of the finger since last frame
        var touchDeltaPosition:Vector2 = Input.GetTouch(0).deltaPosition;

        var touchPosition:Vector3;

        touchPosition.Set(touchDeltaPosition.x, transform.position.y, touchDeltaPosition.y);


        // Move object across XY plane
        transform.position = Vector2(Camera.main.ScreenToWorldPoint(touch.position).x, Camera.main.ScreenToWorldPoint(touch.position).y);
    }
}