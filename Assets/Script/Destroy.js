﻿#pragma strict

function OnTriggerEnter2D( col : Collider2D ){
	if( col.gameObject.tag == "Box1" || col.gameObject.tag == "Box2" || col.gameObject.tag == "Gap1" || col.gameObject.tag == "Gap2" || col.gameObject.tag == "Steel"){
		Destroy(col.gameObject);
	}
}
function OnCollisionEnter2D( col : Collision2D){
	if( col.gameObject.tag == "Box1" || col.gameObject.tag == "Box2" || col.gameObject.tag == "Gap1" || col.gameObject.tag == "Gap2" || col.gameObject.tag == "Steel"){
		Destroy(col.gameObject);
	}
}