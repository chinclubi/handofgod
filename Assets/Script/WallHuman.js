﻿#pragma strict

function Start () {

}

function Update () {

}
function OnTriggerEnter2D(col: Collider2D) {
	if(( col.gameObject.tag == "Player" ) && transform.collider2D.isTrigger){
		transform.collider2D.isTrigger = false;
	}
}
function OnCollisionEnter2D( col : Collision2D){

	if( ( col.gameObject.tag == "Box1" || col.gameObject.tag == "Box2" || col.gameObject.tag == "Steel"  || col.gameObject.tag == "Gap1" || col.gameObject.tag == "Gap2") && !transform.collider2D.isTrigger ){
		transform.collider2D.isTrigger = true;
	}
}