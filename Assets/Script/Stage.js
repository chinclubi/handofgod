﻿#pragma strict

var object1 : Transform;
var object2 : Transform;
var object3 : Transform;
var object4 : Transform;
var object5 : Transform;
var firePillar : Transform;

var pickObj : Transform;
var groundRadius : float;
var Wex : Transform;
var fxWex : Transform;

var topWall : BoxCollider2D;
var bottomWall : BoxCollider2D;
var leftWall : BoxCollider2D;
var rightWall : BoxCollider2D;

var mainCam : Camera;

var cooldown : float = 2f;

function Start () {
	audio.Play();
	InvokeRepeating("CreateDroppings", 1, 2);
	
	topWall.size = new Vector2(mainCam.ScreenToWorldPoint
				  (new Vector3( Screen.width*2, 0f, 0f)).x, 2f);
	topWall.center = new Vector2(0f,mainCam.ScreenToWorldPoint(
				   new Vector3(0f,Screen.height,0f)).y+3f );
	bottomWall.size = new Vector2(mainCam.ScreenToWorldPoint
				  (new Vector3( Screen.width*2, 0f, 0f)).x, 2f);
	bottomWall.center = new Vector2(0f,mainCam.ScreenToWorldPoint(
				   new Vector3(0f,0f,0f)).y-2f );
	leftWall.size = new Vector2(2.5,mainCam.ScreenToWorldPoint
					(new Vector3(1f, Screen.height * 2, 0f)).y);
	leftWall.center = new Vector2(mainCam.ScreenToWorldPoint(
				      new Vector3(0f,0f,0f)).x-2f,0f);
	rightWall.size = new Vector2(2.5f,mainCam.ScreenToWorldPoint
					(new Vector3(1f, Screen.height * 2, 0f)).y);
	rightWall.center = new Vector2(mainCam.ScreenToWorldPoint(
				      new Vector3(Screen.width,0f,0f)).x+2f,0f);
}

function CreateDroppings() {
	var posX = Camera.main.ScreenToWorldPoint(new Vector3( Screen.width, 0f, 0f)).x;

	switch(Random.Range(0,6)){
	case 0:
		Instantiate(object1, Vector3(Random.Range(0 - posX + 0.05f , posX - 0.05f), Camera.main.transform.position.y + 6f, 0), Quaternion.identity);
	break;
	case 1:
		Instantiate(object2, Vector3(Random.Range(0 - posX + 0.05f, posX - 0.05f), Camera.main.transform.position.y + 5f, 0), Quaternion.identity);
	break;
	case 2:
		Instantiate(object3, Vector3(Random.Range(0 - posX + 0.05f, posX - 0.05f), Camera.main.transform.position.y + 5f, 0), Quaternion.identity);
	break;
	case 3:
		Instantiate(object4, Vector3(Random.Range(0 - posX + 0.05f, posX - 0.05f), Camera.main.transform.position.y + 5f, 0), Quaternion.identity);
	break;
	case 4:
		Instantiate(object5, Vector3(Random.Range(0 - posX + 0.05f, posX - 0.05f), Camera.main.transform.position.y + 5f, 0), Quaternion.identity);
	break;
	case 5:
		if(Random.Range(0,4) >= 2) {
			Instantiate(firePillar, Vector3(Random.Range(0 - posX + 0.05f, posX - 0.05f), Camera.main.transform.position.y - 5f, 0), Quaternion.identity);
		} 
	break;
	}
}

function Update () {
	cooldown-= 0.15f;
	for (var i = 0; i < Input.touchCount; ++i) {
   		var hit : RaycastHit2D = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(i).position), Vector2(30f,30f), Mathf.Infinity);
   		var p : Vector3 = Camera.main.ScreenToWorldPoint (Vector3 (Input.GetTouch(i).position.x,Input.GetTouch(i).position.y, 10));
   		if(hit){
   			if(Input.GetTouch(i).phase == TouchPhase.Began){
        		if(hit.collider.tag == "Box1"){
        		}else if(hit.collider.tag == "Box2"){
        			hit.collider.SendMessage("Hit");
        		}else if(hit.collider.tag == "Player"){
        		}else if(hit.collider.tag == "Gap1"){
        		}else if(hit.collider.tag == "Gap2"){
        		}else if( cooldown <= 0f ){
        			Instantiate (Wex, p, Quaternion.identity);
        			Instantiate (fxWex, p, Quaternion.identity);
        			cooldown = 2f;
        		}
        	}else if (Input.GetTouch(i).phase == TouchPhase.Moved){
	   			if(hit.collider.tag == "Box1"){
        			hit.collider.SendMessage("Move",Input.GetTouch(i).deltaPosition);
                }else if(hit.collider.tag == "Box2"){
                	hit.collider.SendMessage("Hit");
                }else if(hit.collider.tag == "Player"){
        		}else if(hit.collider.tag == "Gap1"){
        			if(Input.GetTouch(i).deltaPosition.x > 1f) {
						hit.collider.SendMessage("Destroy");
					}
        		}else if(hit.collider.tag == "Gap2"){
        			if(Input.GetTouch(i).deltaPosition.x > 1f) {
						hit.collider.SendMessage("Destroy");
					}
        		}else{
        		}
        	}else if(Input.GetTouch(i).phase == TouchPhase.Canceled){
        		if(hit.collider.tag == "Box1"){
        		}else if(hit.collider.tag == "Box2"){
        			hit.collider.SendMessage("Hit");
        		}else if(hit.collider.tag == "Player"){
        		}else if(hit.collider.tag == "Gap1"){
        		}else if(hit.collider.tag == "Gap2"){
        		}else{
        		}
        	}else if(Input.GetTouch(i).phase == TouchPhase.Ended){
        		if(hit.collider.tag == "Box1"){
        		}else if(hit.collider.tag == "Box2"){
        			hit.collider.SendMessage("Hit");
        		}else if(hit.collider.tag == "Player"){
        		}else if(hit.collider.tag == "Gap1"){
        		}else if(hit.collider.tag == "Gap2"){
        		}else{
        		}
        	}
        }
	}
}