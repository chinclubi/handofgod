﻿#pragma strict

var topWall : BoxCollider2D;
var bottomWall : BoxCollider2D;
var leftWall : BoxCollider2D;
var rightWall : BoxCollider2D;
var Wex : Transform;


var mainCam : Camera;

function Start () {
	
	topWall.size = new Vector2(mainCam.ScreenToWorldPoint
				  (new Vector3( Screen.width*2, 0f, 0f)).x, 2f);
	topWall.center = new Vector2(0f,mainCam.ScreenToWorldPoint(
				   new Vector3(0f,Screen.height,0f)).y+0.5 );
	bottomWall.size = new Vector2(mainCam.ScreenToWorldPoint
				  (new Vector3( Screen.width*2, 0f, 0f)).x, 2f);
	bottomWall.center = new Vector2(0f,mainCam.ScreenToWorldPoint(
				   new Vector3(0f,0f,0f)).y-0.5f );
	leftWall.size = new Vector2(2.5,mainCam.ScreenToWorldPoint
					(new Vector3(1f, Screen.height * 2, 0f)).y);
	leftWall.center = new Vector2(mainCam.ScreenToWorldPoint(
				      new Vector3(0f,0f,0f)).x-0.5,0f);
	rightWall.size = new Vector2(2.5f,mainCam.ScreenToWorldPoint
					(new Vector3(1f, Screen.height * 2, 0f)).y);
	rightWall.center = new Vector2(mainCam.ScreenToWorldPoint(
				      new Vector3(Screen.width,0f,0f)).x+0.5,0f);
}
function Update () {
	
}
