﻿#pragma strict
var style : GUISkin;
static var scoreCounter : int;
var stoneCount : int;
var plasticCount : int;
var jarCount : int;
var tempTime : float;
var timetime : float;
var tempString : String;
var gameEnded : boolean;

function Start () {
	scoreCounter = 0;
	stoneCount = 8;
	plasticCount = 6;
	jarCount = 14;
	timetime = 10;
	tempTime = Time.time;
	tempString = "";
	gameEnded = false;
	
}

function Update () {
	if (Time.time - tempTime >= 0.04 && !gameEnded)
	{
		scoreCounter++;
		digitCheck();
		tempTime = Time.time;
	}
	else if (timetime - Time.time <= 0)
	{
		//gameEnded = true;
	}
}

function digitCheck() {
	var multiplier : int = 1;
	while (scoreCounter * multiplier < 1000000)
	{
		multiplier *= 10;
	}
	tempString = multiplier + "";
	multiplier = 0;
	tempString = tempString.Substring(1, tempString.Length - 1);
}

function OnGUI() {
	GUI.skin = style;
	var pos : Vector3 = Camera.main.ScreenToWorldPoint(new Vector3( Screen.width, Screen.height, 0f));
//	Debug.Log(pos.x);
	GUI.Label(new Rect( pos.x - 20, pos.y, 500, 200), tempString + scoreCounter );
	if( GUI.Button(new Rect(Screen.width-45,10,45,45),"") ){
	}
	if (gameEnded)
	{
		GUI.Label(new Rect(Screen.width / 2 - 30, Screen.height / 2 + 15, 419, 70), "You swam : " + scoreCounter + " Meters");
		GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 35, 200, 100), stoneCount + " Collected x 100 pts = " + stoneCount * 100);
		GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 65, 200, 100), plasticCount + " Collected x 50 pts = " + plasticCount * 50);
		GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 95, 200, 100), jarCount + " Collected x 100 pts = " + jarCount * 100);
		GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 105, 200, 100), "--------------------------------------------------");
		GUI.Label(new Rect(Screen.width / 2 - 150, Screen.height / 2 + 120, 200, 100), "You got total score of " + (scoreCounter + (stoneCount * 100) + (plasticCount * 50) + (jarCount * 100)));
	}
}